﻿namespace dlgcy
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.comboBox_add = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mainMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemEditFindReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.radioAdd = new System.Windows.Forms.RadioButton();
            this.radioRemove = new System.Windows.Forms.RadioButton();
            this.comboBox_remove = new System.Windows.Forms.ComboBox();
            this.radioReverse = new System.Windows.Forms.RadioButton();
            this.radio1 = new System.Windows.Forms.RadioButton();
            this.radio2 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_SplitStr = new System.Windows.Forms.ComboBox();
            this.comboBox_StartNum = new System.Windows.Forms.ComboBox();
            this.radioRemoveOrder = new System.Windows.Forms.RadioButton();
            this.radioOrder = new System.Windows.Forms.RadioButton();
            this.radioSplit = new System.Windows.Forms.RadioButton();
            this.groupBox_SplitType = new System.Windows.Forms.GroupBox();
            this.radioSplitLeft = new System.Windows.Forms.RadioButton();
            this.radioSplitRight = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox_SplitType.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(6, 78);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1166, 511);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            this.richTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBox1_MouseClick);
            this.richTextBox1.TextChanged += new System.EventHandler(this.RichTextBox1_TextChanged);
            // 
            // comboBox_add
            // 
            this.comboBox_add.FormattingEnabled = true;
            this.comboBox_add.Items.AddRange(new object[] {
            "《》",
            "“”",
            "‘’",
            "＂＂",
            "<>",
            "﹛﹜",
            "〔〕",
            "［］",
            "【】",
            "〈〉",
            "『』",
            "「」"});
            this.comboBox_add.Location = new System.Drawing.Point(12, 51);
            this.comboBox_add.Name = "comboBox_add";
            this.comboBox_add.Size = new System.Drawing.Size(88, 20);
            this.comboBox_add.TabIndex = 0;
            this.comboBox_add.Text = "《》";
            this.comboBox_add.TextChanged += new System.EventHandler(this.ComboBox_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuFile,
            this.menuItemEdit,
            this.menuItemHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 25);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mainMenuFile
            // 
            this.mainMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemFileNew,
            this.menuItemFileOpen,
            this.menuItemFileSave,
            this.menuItemFileSaveAs,
            this.menuItemFileExit});
            this.mainMenuFile.Name = "mainMenuFile";
            this.mainMenuFile.Size = new System.Drawing.Size(44, 21);
            this.mainMenuFile.Text = "文件";
            // 
            // menuItemFileNew
            // 
            this.menuItemFileNew.Name = "menuItemFileNew";
            this.menuItemFileNew.Size = new System.Drawing.Size(121, 22);
            this.menuItemFileNew.Text = "新建";
            this.menuItemFileNew.Click += new System.EventHandler(this.MenuItemFileNew_Click);
            // 
            // menuItemFileOpen
            // 
            this.menuItemFileOpen.Name = "menuItemFileOpen";
            this.menuItemFileOpen.Size = new System.Drawing.Size(121, 22);
            this.menuItemFileOpen.Text = "打开...";
            this.menuItemFileOpen.Click += new System.EventHandler(this.MenuItemFileOpen_Click);
            // 
            // menuItemFileSave
            // 
            this.menuItemFileSave.Name = "menuItemFileSave";
            this.menuItemFileSave.Size = new System.Drawing.Size(121, 22);
            this.menuItemFileSave.Text = "保存...";
            this.menuItemFileSave.Click += new System.EventHandler(this.MenuItemFileSave_Click);
            // 
            // menuItemFileSaveAs
            // 
            this.menuItemFileSaveAs.Name = "menuItemFileSaveAs";
            this.menuItemFileSaveAs.Size = new System.Drawing.Size(121, 22);
            this.menuItemFileSaveAs.Text = "另存为...";
            this.menuItemFileSaveAs.Click += new System.EventHandler(this.MenuItemFileSaveAs_Click);
            // 
            // menuItemFileExit
            // 
            this.menuItemFileExit.Name = "menuItemFileExit";
            this.menuItemFileExit.Size = new System.Drawing.Size(121, 22);
            this.menuItemFileExit.Text = "退出";
            this.menuItemFileExit.Click += new System.EventHandler(this.MenuItemExit_Click);
            // 
            // menuItemEdit
            // 
            this.menuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemEditFindReplace});
            this.menuItemEdit.Name = "menuItemEdit";
            this.menuItemEdit.Size = new System.Drawing.Size(44, 21);
            this.menuItemEdit.Text = "编辑";
            // 
            // menuItemEditFindReplace
            // 
            this.menuItemEditFindReplace.Name = "menuItemEditFindReplace";
            this.menuItemEditFindReplace.Size = new System.Drawing.Size(136, 22);
            this.menuItemEditFindReplace.Text = "查找与替换";
            this.menuItemEditFindReplace.Click += new System.EventHandler(this.MenuItemFindReplace_Click);
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemHelpAbout});
            this.menuItemHelp.Name = "menuItemHelp";
            this.menuItemHelp.Size = new System.Drawing.Size(44, 21);
            this.menuItemHelp.Text = "帮助";
            // 
            // menuItemHelpAbout
            // 
            this.menuItemHelpAbout.Name = "menuItemHelpAbout";
            this.menuItemHelpAbout.Size = new System.Drawing.Size(109, 22);
            this.menuItemHelpAbout.Text = "关于...";
            this.menuItemHelpAbout.Click += new System.EventHandler(this.MenuItemAbout_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.FileName = "*.txt";
            this.openFileDialog1.Filter = "文本文件|*.txt|所有文件|*.*";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "txt";
            this.saveFileDialog1.Filter = "文本文件|*.txt|所有文件|*.*";
            // 
            // radioAdd
            // 
            this.radioAdd.AutoSize = true;
            this.radioAdd.Checked = true;
            this.radioAdd.Location = new System.Drawing.Point(12, 29);
            this.radioAdd.Name = "radioAdd";
            this.radioAdd.Size = new System.Drawing.Size(71, 16);
            this.radioAdd.TabIndex = 1;
            this.radioAdd.TabStop = true;
            this.radioAdd.Tag = "";
            this.radioAdd.Text = "添加符号";
            this.radioAdd.UseVisualStyleBackColor = true;
            this.radioAdd.CheckedChanged += new System.EventHandler(this.RadioCheckedChanged);
            // 
            // radioRemove
            // 
            this.radioRemove.AutoSize = true;
            this.radioRemove.Location = new System.Drawing.Point(208, 29);
            this.radioRemove.Name = "radioRemove";
            this.radioRemove.Size = new System.Drawing.Size(71, 16);
            this.radioRemove.TabIndex = 4;
            this.radioRemove.TabStop = true;
            this.radioRemove.Tag = "";
            this.radioRemove.Text = "移除符号";
            this.radioRemove.UseVisualStyleBackColor = true;
            this.radioRemove.CheckedChanged += new System.EventHandler(this.RadioCheckedChanged);
            // 
            // comboBox_remove
            // 
            this.comboBox_remove.Enabled = false;
            this.comboBox_remove.FormattingEnabled = true;
            this.comboBox_remove.Items.AddRange(new object[] {
            "《》",
            "“”",
            "‘’",
            "＂＂",
            "<>",
            "﹛﹜",
            "〔〕",
            "［］",
            "【】",
            "〈〉",
            "『』",
            "「」"});
            this.comboBox_remove.Location = new System.Drawing.Point(208, 51);
            this.comboBox_remove.Name = "comboBox_remove";
            this.comboBox_remove.Size = new System.Drawing.Size(88, 20);
            this.comboBox_remove.TabIndex = 5;
            this.comboBox_remove.Text = "《》";
            this.comboBox_remove.TextChanged += new System.EventHandler(this.ComboBox_TextChanged);
            // 
            // radioReverse
            // 
            this.radioReverse.AutoSize = true;
            this.radioReverse.Location = new System.Drawing.Point(312, 29);
            this.radioReverse.Name = "radioReverse";
            this.radioReverse.Size = new System.Drawing.Size(71, 16);
            this.radioReverse.TabIndex = 6;
            this.radioReverse.TabStop = true;
            this.radioReverse.Tag = "";
            this.radioReverse.Text = "行数逆转";
            this.radioReverse.UseVisualStyleBackColor = true;
            this.radioReverse.CheckedChanged += new System.EventHandler(this.RadioCheckedChanged);
            // 
            // radio1
            // 
            this.radio1.AutoSize = true;
            this.radio1.Checked = true;
            this.radio1.Location = new System.Drawing.Point(9, 11);
            this.radio1.Name = "radio1";
            this.radio1.Size = new System.Drawing.Size(71, 16);
            this.radio1.TabIndex = 2;
            this.radio1.TabStop = true;
            this.radio1.Tag = "";
            this.radio1.Text = "单次模式";
            this.radio1.UseVisualStyleBackColor = true;
            // 
            // radio2
            // 
            this.radio2.AutoSize = true;
            this.radio2.Location = new System.Drawing.Point(9, 28);
            this.radio2.Name = "radio2";
            this.radio2.Size = new System.Drawing.Size(71, 16);
            this.radio2.TabIndex = 3;
            this.radio2.TabStop = true;
            this.radio2.Tag = "";
            this.radio2.Text = "多行模式";
            this.radio2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radio1);
            this.groupBox1.Controls.Add(this.radio2);
            this.groupBox1.Location = new System.Drawing.Point(103, 24);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(92, 47);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // comboBox_SplitStr
            // 
            this.comboBox_SplitStr.Enabled = false;
            this.comboBox_SplitStr.FormattingEnabled = true;
            this.comboBox_SplitStr.Items.AddRange(new object[] {
            "、",
            " ",
            ".",
            ","});
            this.comboBox_SplitStr.Location = new System.Drawing.Point(483, 52);
            this.comboBox_SplitStr.Name = "comboBox_SplitStr";
            this.comboBox_SplitStr.Size = new System.Drawing.Size(73, 20);
            this.comboBox_SplitStr.TabIndex = 19;
            this.comboBox_SplitStr.Text = "、";
            // 
            // comboBox_StartNum
            // 
            this.comboBox_StartNum.Enabled = false;
            this.comboBox_StartNum.FormattingEnabled = true;
            this.comboBox_StartNum.Items.AddRange(new object[] {
            "1"});
            this.comboBox_StartNum.Location = new System.Drawing.Point(404, 52);
            this.comboBox_StartNum.Name = "comboBox_StartNum";
            this.comboBox_StartNum.Size = new System.Drawing.Size(73, 20);
            this.comboBox_StartNum.TabIndex = 18;
            this.comboBox_StartNum.Text = "1";
            // 
            // radioRemoveOrder
            // 
            this.radioRemoveOrder.AutoSize = true;
            this.radioRemoveOrder.Location = new System.Drawing.Point(483, 29);
            this.radioRemoveOrder.Name = "radioRemoveOrder";
            this.radioRemoveOrder.Size = new System.Drawing.Size(71, 16);
            this.radioRemoveOrder.TabIndex = 17;
            this.radioRemoveOrder.TabStop = true;
            this.radioRemoveOrder.Tag = "";
            this.radioRemoveOrder.Text = "移除序号";
            this.radioRemoveOrder.UseVisualStyleBackColor = true;
            this.radioRemoveOrder.CheckedChanged += new System.EventHandler(this.RadioCheckedChanged);
            // 
            // radioOrder
            // 
            this.radioOrder.AutoSize = true;
            this.radioOrder.Location = new System.Drawing.Point(404, 29);
            this.radioOrder.Name = "radioOrder";
            this.radioOrder.Size = new System.Drawing.Size(71, 16);
            this.radioOrder.TabIndex = 16;
            this.radioOrder.TabStop = true;
            this.radioOrder.Tag = "";
            this.radioOrder.Text = "添加序号";
            this.radioOrder.UseVisualStyleBackColor = true;
            this.radioOrder.CheckedChanged += new System.EventHandler(this.RadioCheckedChanged);
            // 
            // radioSplit
            // 
            this.radioSplit.AutoSize = true;
            this.radioSplit.Location = new System.Drawing.Point(571, 54);
            this.radioSplit.Name = "radioSplit";
            this.radioSplit.Size = new System.Drawing.Size(83, 16);
            this.radioSplit.TabIndex = 20;
            this.radioSplit.TabStop = true;
            this.radioSplit.Tag = "";
            this.radioSplit.Text = "截除字符串";
            this.radioSplit.UseVisualStyleBackColor = true;
            // 
            // groupBox_SplitType
            // 
            this.groupBox_SplitType.Controls.Add(this.radioSplitLeft);
            this.groupBox_SplitType.Controls.Add(this.radioSplitRight);
            this.groupBox_SplitType.Enabled = false;
            this.groupBox_SplitType.Location = new System.Drawing.Point(659, 24);
            this.groupBox_SplitType.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox_SplitType.Name = "groupBox_SplitType";
            this.groupBox_SplitType.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox_SplitType.Size = new System.Drawing.Size(92, 47);
            this.groupBox_SplitType.TabIndex = 12;
            this.groupBox_SplitType.TabStop = false;
            // 
            // radioSplitLeft
            // 
            this.radioSplitLeft.AutoSize = true;
            this.radioSplitLeft.Checked = true;
            this.radioSplitLeft.Location = new System.Drawing.Point(9, 11);
            this.radioSplitLeft.Name = "radioSplitLeft";
            this.radioSplitLeft.Size = new System.Drawing.Size(71, 16);
            this.radioSplitLeft.TabIndex = 2;
            this.radioSplitLeft.TabStop = true;
            this.radioSplitLeft.Tag = "";
            this.radioSplitLeft.Text = "截掉左边";
            this.radioSplitLeft.UseVisualStyleBackColor = true;
            // 
            // radioSplitRight
            // 
            this.radioSplitRight.AutoSize = true;
            this.radioSplitRight.Location = new System.Drawing.Point(9, 28);
            this.radioSplitRight.Name = "radioSplitRight";
            this.radioSplitRight.Size = new System.Drawing.Size(71, 16);
            this.radioSplitRight.TabIndex = 3;
            this.radioSplitRight.TabStop = true;
            this.radioSplitRight.Tag = "";
            this.radioSplitRight.Text = "截掉右边";
            this.radioSplitRight.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 601);
            this.Controls.Add(this.groupBox_SplitType);
            this.Controls.Add(this.radioSplit);
            this.Controls.Add(this.comboBox_SplitStr);
            this.Controls.Add(this.comboBox_StartNum);
            this.Controls.Add(this.radioRemoveOrder);
            this.Controls.Add(this.radioOrder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radioReverse);
            this.Controls.Add(this.comboBox_remove);
            this.Controls.Add(this.radioRemove);
            this.Controls.Add(this.radioAdd);
            this.Controls.Add(this.comboBox_add);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.richTextBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "符号包裹";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox_SplitType.ResumeLayout(false);
            this.groupBox_SplitType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ComboBox comboBox_add;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFile;
        private System.Windows.Forms.ToolStripMenuItem menuItemFileNew;
        private System.Windows.Forms.ToolStripMenuItem menuItemFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuItemFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuItemFileSaveAs;
        private System.Windows.Forms.ToolStripMenuItem menuItemFileExit;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem menuItemHelp;
        private System.Windows.Forms.ToolStripMenuItem menuItemHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem menuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem menuItemEditFindReplace;
        private System.Windows.Forms.RadioButton radioAdd;
        private System.Windows.Forms.RadioButton radioRemove;
        private System.Windows.Forms.ComboBox comboBox_remove;
        private System.Windows.Forms.RadioButton radioReverse;
        private System.Windows.Forms.RadioButton radio1;
        private System.Windows.Forms.RadioButton radio2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_SplitStr;
        private System.Windows.Forms.RadioButton radioRemoveOrder;
        private System.Windows.Forms.RadioButton radioOrder;
        private System.Windows.Forms.ComboBox comboBox_StartNum;
        private System.Windows.Forms.RadioButton radioSplit;
        private System.Windows.Forms.GroupBox groupBox_SplitType;
        private System.Windows.Forms.RadioButton radioSplitLeft;
        private System.Windows.Forms.RadioButton radioSplitRight;
    }
}

