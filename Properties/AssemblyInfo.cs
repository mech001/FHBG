﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("符号包裹")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("独立观察员")]
[assembly: AssemblyProduct("符号包裹")]
[assembly: AssemblyCopyright("Copyright © 2014-2020")]
[assembly: AssemblyTrademark("DLGCY")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDescription
    (@"    详情请见 http://dlgcy.com/fhbg

    ------------------ Version 1.6 -----------------
    增加替换掉换行功能.

    ------------------ Version 1.5 -----------------
    增加替换为换行功能.

    ------------------ Version 1.4 -----------------
    增加全部替换功能.

    ------------------ Version 1.3 -----------------
    序号功能和截断字符串功能.

    ------------------ Version 1.2 -----------------
    使内容输入框能跟随窗口大小.

    ------------------------------------------------")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("b73456cc-b14d-4154-a4a0-09a9da8d99d9")]

// 程序集的版本信息由下面四个值组成: 
//
//      主版本
//      次版本 
//      生成号
//      修订号
//
// 可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”: 
[assembly: AssemblyVersion("1.6.*")]
//[assembly: AssemblyFileVersion("1.3")]  //注释此句使文件属性中的文件版本取值AssemblyVersion;
[assembly: AssemblyInformationalVersion("1.6 增加替换掉换行功能")]