﻿using System;
using System.Windows.Forms;

namespace dlgcy
{
    public partial class formFindReplace : Form
    {
        MainForm MainForm1; 
        public formFindReplace(MainForm mainForm)
        {
            InitializeComponent();
            MainForm1 = mainForm;
        }

        private void formFindReplace_Load(object sender, EventArgs e)
        {

        }
        private void buttonFind_Click(object sender,EventArgs e)
        {	
            if(TBFind.Text.Length!=0)
                MainForm1.FindRichTextBoxString(TBFind.Text); 
            else
                MessageBox.Show("查找字符串不能为空","提示",MessageBoxButtons.OK);
        }

        private void buttonReplace_Click(object sender, EventArgs e)
        {
            if (TBReplace.Text.Length != 0)
                MainForm1.ReplaceRichTextBoxString(TBReplace.Text);
            else
                MessageBox.Show("替换字符串不能为空", "提示", MessageBoxButtons.OK);
        }

        private void BtnReplaceAll_Click(object sender, EventArgs e)
        {
            if (TBFind.Text.Length != 0)
                MainForm1.ReplaceRichTextBoxStringAll(TBFind.Text, TBReplace.Text);
            else
                MessageBox.Show("查找字符串不能为空", "提示", MessageBoxButtons.OK);
        }

        private void CBChangeToBr_CheckedChanged(object sender, EventArgs e)
        {
            if (CBChangeToBr.Checked)
            {
                TBReplace.Text = "" + (char)10;
                TBReplace.Enabled = false;
            }
            else
            {
                TBReplace.Text = "";
                TBReplace.Enabled = true;
            }
        }

        private void CBReplaceBr_CheckedChanged(object sender, EventArgs e)
        {
            if (CBReplaceBr.Checked)
            {
                TBFind.Text = "" + (char)10;
                TBFind.Enabled = false;
            }
            else
            {
                TBFind.Text = "";
                TBFind.Enabled = true;
            }
        }
    }
}
